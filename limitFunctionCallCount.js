function limitFunctionCallCount(cb,n){
    
    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as first argument')
        return
    }

    if(n<=0){
        return 
    }

    return ()=>{
        for(let i=1;i<n+1;i++){
            cb(i)
        }
    }
}

module.exports = limitFunctionCallCount
