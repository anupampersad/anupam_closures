function cacheFunction(cb) {

    if (typeof(cb)!=='function'){
        console.log('Please pass a call back function as second parameter')
        return
    }

    let cache = {}

    return (n) => {

        if (n in cache) {
            // console.log('already present in cache ')
            console.log(`argument ${n} has already been passed before and the result is : `)
            return cache[n]
        }
        else {
            cache[n] = cb(n)
            // console.log('coming first time')
            console.log(`argument ${n} is coming for first time. Result is : `)
            return cache[n]
        }

    }

}

module.exports = cacheFunction

