const limitFunctionCallCount = require('../limitFunctionCallCount')

function callBack(val){
    console.log(`Called ${val} times`)
}

let x = limitFunctionCallCount(callBack,15)
x()