let count = 133

function counterFactory(){

    let object = {

        increment: () =>{
            count+=1
            return count
        },

        decrement: () =>{
            count-=1
            return count
        },

        presentValue: () =>{
            return count
        }

    }

    return object
}

module.exports = counterFactory


